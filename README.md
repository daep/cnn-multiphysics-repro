# cnn-multiphysics-repro

Repository is organized as follows:

- [images](./images): folder containing the figures shown on this page
- [network](./network): implementation of the neural network, train and testing scripts
- [samples](./samples): sample of trained models for the different physics and floating-point precisions
- [simulation](./simulation): code for the generation of the databases

More details are availble in the subfolders. 

Network architecture
------------

Neural network is multi-scale (field dimensions of N, N/2 and N/4), composed by 17 two-dimensional convolution operations, for a total of 422,419 trainable parameters. ReLUs are used as activation function and replication padding is used to maintain layers size unchanged inside each scale.

<p align="center">
  <img alt="Neural network architecture" src="./images/drawing_network_architecture.png" width="800"/>
</p>

Computing environment
------------

Hardware and versions of the software used in the study are listed below:

+ CPU: Intel® Xeon® Gold 6126 Processor 2.60 GHz
+ GPU: Nvidia Tesla V100 32Gb, Nvidia GPU Driver Version 455.32
+ Software: [Python](https://www.python.org/) 3.8.5, [PyTorch](https://pytorch.org/) 1.7.0, [pytorch-lightning](https://www.pytorchlightning.ai/) 1.0.1, [numpy](https://numpy.org/) 1.19.2 and [CUDA](https://developer.nvidia.com/cuda-toolkit) 11.1

