#!/bin/bash
################################################################################
#                                                                              #
# Script for generating a database with the Burgers' equation.                 #
# Sole argument is the parameters' file path. Example of command:              #
#                                                                              #
#   >>> bash generate.sh /path/to/parameters                                   #
#                                                                              #
# Multiple background jobs are going to be called to allow a faster            #
# generation, control it with the NUMBER_JOBS on parameter's file. For more    #
# details, read the sample PARAMETERS file.                                    #
#                                                                              #
################################################################################

SCRIPT=$( readlink -f "$0" )
BASEDIR=$( realpath $(dirname "$SCRIPT") ) # script folder

# add folder with executable
PATH="${PATH}:${BASEDIR}/bin"

temp_folder_name='.tmp_burgers'
parameters_file=$( realpath $1 )

###########
# Function for reading a named value in parameters' file
#
# Arguments:
#
#   - var_name: string with the name of the parameter
#
read_parameter()
{
    var_name=$1
    full_line=$( grep "${var_name}=" ${parameters_file} )
    line=(${full_line//#/ }) # ignoring inline comments
    parameter=(${line[0]//=/ })
    echo ${parameter[1]}
} 

data_folder=$( read_parameter "DATA_FOLDER" )
simu_folder=$( realpath $( read_parameter "SIMU_FOLDER" ) )
template_folder=$( read_parameter "TEMPLATE_FOLDER" )
n_cases=$( read_parameter "NUMBER_CASES" )
n_jobs=$( read_parameter "NUMBER_JOBS" )
delta_T=$( read_parameter "DELTAT" )

# remove previously generated temporary folders, if they exist
echo -e "Clean previous temporary folders...\n"
for folder in "${simu_folder}/${temp_folder_name}*"
do echo "Deleting ${folder}"; rm -rf $folder; done

# create destination folder
mkdir -p $data_folder
data_folder=$( realpath $data_folder ) # get absolute path

# create a copy of the case template
i_job=1
temp_folder_init="${simu_folder}/${temp_folder_name}${i_job}"
cp -rf "${BASEDIR}/${template_folder}" ${temp_folder_init}

###########
# reading parameters file, update the template configuration files and
# generates the mesh
echo -e "\nReading parameters:\n"
cd "${temp_folder_init}"
while IFS= read -r full_line
do
    case "$full_line" in \#*)
        continue ; # skip lines of comment
    esac
        # ignoring the comments
        line=(${full_line//#/ })
        parameter=(${line[0]//=/ })
        echo "$line"
        key=${parameter[0]} 
        value=${parameter[1]}
        
        # replacing the properties values for each file on initial
        # temporary case folder
        find ./ -type f -exec sed -i 's|!'"$key"'!|'"$value"'|g' {} +

done < "$parameters_file"
cd ${BASEDIR}

# making multiple copies of the temporary case and meshing
echo -e "\nMeshing cases..."
for i_job in $(seq 1 $n_jobs)
do
    temp_folder="${simu_folder}/${temp_folder_name}${i_job}"
    # not necessary to copy the first, only to mesh
    if (( i_job > 1 ))
        # do nothing
    then
        cp -r "${temp_folder_init}" "${temp_folder}"
    fi

    echo "$i_job"
    cd "${temp_folder}"; \
        blockMesh &> "${simu_folder}/log.tmp${i_job}"; \
        cd $BASEDIR &
done
wait
echo -e "Done meshing\n"

###########
# Function for performing a simulation
#
# Arguments:
# 
#    - job_index: integer that identifies the temporary case folder
#    - case_index: integer used to generate a custom random number seed
#    - case_name: unique case name, used as name of folder
#
simulate_case()
{
    job_index=$1
    case_index=$2
    case_name=$3

    temp_folder="${simu_folder}/${temp_folder_name}${job_index}"

    cd "${temp_folder}"

    # changing the random number generation seed based on case index
    # using the time as a mean to have custom seeds for each run
    date_seconds=$( date +%s/%S )
    seed=$( echo "${date_seconds}/16000000 + ${case_index}" | bc -l )
    sed -i 's|const scalar seed = .*;|'"const scalar seed = ${seed};"'|g' \
        "${temp_folder}/0/U"

    # running the simulation
    burgersFoam &>> "${simu_folder}/log.tmp${job_index}"
    # extracting the fields
    postProcess -func surfaces &>> "${simu_folder}/log.tmp${job_index}"

    # copying simulation files to the destination folder 
    case_folder="${data_folder}/${case_name}"
    mkdir -p $case_folder
    for time_folder in postProcessing/surfaces/*
    do
        time="${time_folder##*/}"
        timestep=$( bc -l <<< "${time} / ${delta_T}" )
        timestep_string=$(LC_NUMERIC=C printf "%06.0f" $timestep)
        
        cp "${time_folder}/surface.vtp" \
           "${case_folder}/vtk${timestep_string}.vtp"
    done
}

# loop of parallel cases
case_index=-1
echo -e "\nLaunching calculations..."
number_loops=$(( $n_cases / $n_jobs ))

for i_loop in $(seq 1 $number_loops)
do
    # loop for launching the parallel jobs, one per temporary case folder
    for i_job in $(seq 1 $n_jobs)
    do
        case_index=$( expr ${case_index} + 1 )
        echo "($i_loop, $i_job): $case_index "
        case_name=$(LC_NUMERIC=C printf "%06d" $case_index)

        # call function with simulation + post processing in background
        simulate_case $i_job $case_index $case_name &
    done
    wait
done

echo -e "Done simulations\n"
cd ${BASEDIR}

# remove temporary folders
echo -e "Clean temporary folders...\n"
for folder in "${simu_folder}/${temp_folder_name}*"
do echo "Deleting ${folder}"; rm -rf $folder; done

cd ${BASEDIR}