# Simulation of Burgers' equation

## Dataset generation

This folder contains a solver (C++) script to solve the Burgers' equation. It relies on the open-source finite-volumes framework OpenFOAM.

Compiling the solver 
------------

For using it, OpenFOAM must be installed and loaded. The code presented here was only tested on ESI OpenCFD's (openfoam.com) version, releases v1906 and v2012.

Follow the instructions available on [openfoam.com](https://www.openfoam.com/news/main-news/openfoam-v20-12) or [openfoam.org](https://openfoam.org/download/) or load it in your cluster (`module load openfoam`).

Source code is available at `src` folder. In order to compile the solvers, go to such folder and run the command `wmake`. Cleaning any cache from previous compilation is recommended with `wclean`. For example, considering an Unix system:

<pre style="background-color:#cff0fa"><code>mkdir bin
cd src
wclean
wmake
</code></pre>

By default, the binary is generated at the `bin` folder, ignored by git. To change the name and location of the binary you can modify the value of `EXE` in `files` on subfolder `src/Make`.

Generating the databases
------------

For performing the simulations, do the following command:

<pre style="background-color:#cff0fa"><code>bash generate.sh <param></code></pre>

where `param` is a file with the database and simulation properties.
One can create a custom parameters input file. Present examples will reproduce the physical properties and the database proportions used in the article:

+ `generate_database_train` - training database, with 400 simulations of random pulses (1 to 4)
+ `generate_database_valid` - validation database, with 100 simulations of random pulses (1 to 4)
+ `generate_database_test` - testing database, with 100 simulations of random pulses (1 to 4)
+ `generate_benchmark_pulse` - Gaussian pulse benchmark on the center of the domain
+ `generate_benchmark_opposedGaussians` - opposed Gaussians benchmark at the horizontal symmetry axis of the domain
+ `generate_benchmark_sineWave` - sine wave.

If you want to modify or create your own cases, check the folder in `cases`.The initial fields are completely defined in OpenFOAM's dictionary file `cases/<case>/0/U`. Subfolder `templates` contains the base configuration, with the random pulses. 

Software versions
------------

[Intel® C++ Compiler](https://software.intel.com/content/www/us/en/develop/documentation/cpp-compiler-developer-guide-and-reference/top.html) 18.2, [Intel® MPI Library](https://software.intel.com/content/www/us/en/develop/documentation/mpi-developer-guide-linux/top.html) 18.2, [OpenFOAM](https://www.openfoam.com) v1906
