/* 
 * Code to simulate the dissipation of temperature pulses in an adiabatic
 # rectangular domain.
 *
 * This file is based on the Palabos library.
 *
 * The most recent release of Palabos can be downloaded at 
 * <http://www.palabos.org/>
 *
 * The library Palabos is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "palabos2D.h"
#include "palabos2D.hh"

#include <vector>
#include <iomanip>
#include <iostream>
#include <sstream>

#include <random>
#include <sys/stat.h> // To create output folder.

using namespace plb;
using namespace std;

typedef double T;
#define DESCRIPTOR descriptors::AdvectionDiffusionD2Q5Descriptor
#define DYNAMICS AdvectionDiffusionBGKdynamics

struct Param
{
    /*
     * User defined parameters.
     */

    // Random numbers generator
    plint randomSeed;
    
    // Geometry.
    // Size of computational domain, in physical units.
    T lx;                                            
    T ly;
    
    // Temperature field
    // Average temperature.
    T T0;                                           

    // Gaussian pulses (Defined by user).
    plint numUserPulses;                             // Number user-defined pulses. 
    std::vector<T> userCx;                           // Position of the pulse center, in physical units.
    std::vector<T> userCy;                           // Position of the pulse center, in physical units.
    std::vector<T> userEps;                          // Gaussian pulse amplitude with respect to offset. 
    std::vector<T> userB;                            // Gaussian pulse half-width, in physical units. 
        
    // Randomly generated gaussian pulses.
    Array<plint, 2> statNumRandPulses;               // Number of randomly generated pulses. 
    Array<T,2> statCx;                               // Position of the pulse center, in physical units.
    Array<T,2> statCy;                               // Position of the pulse center, in physical units.
    Array<T,2> statEps;                              // Gaussian pulse amplitude with respect to offset. 
    Array<T,2> statB;                                // Gaussian pulse half-width, in physical units. 

    // Numerics.
    T nu;                                            // Kinematic viscosity, in physical units.
    plint resolution;                                // Number of lattice nodes along a reference length.
    
    // Output
    T maxT;                                          // Time for events in lattice units.
    std::string outputDir;                           // Output Directory
    std::string outputMode;                          // Output Mode: full or cropped vtk
    plint maxIter;                                   // Time for events in lattice units.
    plint vtkIter;                                   // Time for vtk output.
    plint numSim;                                    // Number of simulations.

    /*
     * Derived parameters.
     */
    T dx;                                            // Discrete space step.
    T dt;

    Array<T,2> physicalLocation;

    plint nx;                                        // Grid resolution of bounding box.
    plint nx_domain;                                 // Grid resolution of physcial domain (excluding sponge zones).
    plint ny;                                        // Grid resolution of bounding box.
    plint ny_domain;                                 // Grid resolution of physcial domain (excluding sponge zones).
    Box2D physdomain;                                // box defining the physical domain (excluding sponges)
    Box2D fulldomain;                                // box defining the whole domain (including sponge zones).

    T nuLB;                                          // Kinematic viscosity, in lattice units.
    T omega;                                         // LB relaxation parameter.
    
    // Pulses
    plint numRandPulses;
    plint totalNumPulses;
    std::vector<T> cx;                               // Position of the pulse center, in physical units.
    std::vector<T> cy;                               // Position of the pulse center, in physical units.
    std::vector<T> eps;                              // Gaussian pulse amplitude with respect to offset. 
    std::vector<T> b;                                // Gaussian pulse half-width, in physical units. 
    std::string pulseType;
    
    std::vector<T> cxLB;                             // Position of the pulse center, in LB units.
    std::vector<T> cyLB;                             // Position of the pulse center, in LB units.
    
    Param()
    { }

    Param(std::string xmlFname)
    {

        XMLreader document(xmlFname);
	    document["param"]["randomSeed"].read(randomSeed);
        
	    document["param"]["geometry"]["lx"].read(lx);
        document["param"]["geometry"]["ly"].read(ly);
        physicalLocation = {0.0, 0.0};

        document["param"]["heat"]["T0"].read(T0);
        document["param"]["heat"]["type"].read(pulseType);

        document["param"]["heat"]["pulse"]["userDefined"]["numUserPulses"].read(numUserPulses);
        PLB_ASSERT(numUserPulses >= 0);

        if (numUserPulses > 0) 
        {
            std::vector<T> x, y, k, l;
            document["param"]["heat"]["pulse"]["userDefined"]["center"]["x"].read(x);
            document["param"]["heat"]["pulse"]["userDefined"]["center"]["y"].read(y);
            document["param"]["heat"]["pulse"]["userDefined"]["amplitude"].read(k);
            document["param"]["heat"]["pulse"]["userDefined"]["halfWidth"].read(l);
            PLB_ASSERT(x.size() == numUserPulses && y.size() == numUserPulses);
            PLB_ASSERT(k.size() == numUserPulses && l.size() == numUserPulses);
            userCx.resize(numUserPulses);
            userCy.resize(numUserPulses);
            userEps.resize(numUserPulses);
            userB.resize(numUserPulses);
            for (plint i = 0; i < numUserPulses; i++) {
                userCx[i] = x[i];
                userCy[i] = y[i];
                userEps[i] = k[i];
                userB[i] = l[i];
            }
            
        }

        {
            std::vector<plint> i;
            std::vector<T> u, v, x, y;
            document["param"]["heat"]["pulse"]["randomDefined"]["numRandPulses"].read(i);
            document["param"]["heat"]["pulse"]["randomDefined"]["center"]["x"].read(u);
            document["param"]["heat"]["pulse"]["randomDefined"]["center"]["y"].read(v);
            document["param"]["heat"]["pulse"]["randomDefined"]["amplitude"].read(x);
            document["param"]["heat"]["pulse"]["randomDefined"]["halfWidth"].read(y);

            for (plint j = 0; j < 2; j++) {
                statNumRandPulses[j] = i[j];
                statCx[j] = u[j];
                statCy[j] = v[j];
                statEps[j] = x[j];
                statB[j] = y[j];

            }
        }
        
        document["param"]["numerics"]["nu"].read(nu);
        document["param"]["numerics"]["resolution"].read(resolution);      

        document["param"]["output"]["outputDir"].read(outputDir);
        document["param"]["output"]["outputMode"].read(outputMode);
        document["param"]["output"]["maxT"].read(maxT);
        document["param"]["output"]["vtkIter"].read(vtkIter);
        document["param"]["output"]["numSim"].read(numSim);

    }

};

T toPhys(T lbVal, plint direction, T dx, Array<T,2> const& location)
{
    PLB_ASSERT(direction >= 0 && direction <= 1);
    return(lbVal * dx + location[direction]);
}

Array<T,2> toPhys(Array<T,2> const& lbVal, T dx, Array<T,2> const& location)
{
    return(lbVal * dx + location);
}

T toLB(T physVal, plint direction, T dx, Array<T,2> const& location)
{
    PLB_ASSERT(direction >= 0 && direction <= 1);
    return((physVal - location[direction]) / dx);
}

Array<T,2> toLB(Array<T,2> const& physVal, T dx, Array<T,2> const& location)
{
    return((physVal - location) / dx);
}

void computeDerivedParameters(Param& param)
{
    // Discrete space step
    param.dx = param.ly / (param.resolution);
    param.dt = (T) 1;
    
    // Kinematic viscosity, in lattice units.
    param.nuLB = param.nu * param.dt/(param.dx*param.dx);
    // Relaxation parameter
    param.omega = 1.0/(DESCRIPTOR<T>::invCs2*param.nuLB+0.5);
    
    // Grid resolution of bounding box.
    param.nx = util::roundToInt(param.lx/param.dx);
    param.nx_domain = param.nx;
    param.ny = util::roundToInt(param.ly/param.dx);
    param.ny_domain = param.ny;
   
    param.cxLB.resize(param.totalNumPulses);
    param.cyLB.resize(param.totalNumPulses);
    for (plint i = 0; i < param.totalNumPulses; i++) {
        param.cxLB[i] = toLB(param.cx[i], 0, param.dx, param.physicalLocation);
        param.cyLB[i] = toLB(param.cy[i], 1, param.dx, param.physicalLocation);
    }

    // Time for events in lattice units.
    param.maxIter = util::roundToInt(param.maxT/param.dt);       

    std::string temperature_name = "temperature";

}

void printParam(Param& param)
{

    pcout << "User defined parameters: " <<std::endl;
    pcout << "lx = " << param.lx << std::endl;
    pcout << "ly = " << param.ly << std::endl << std::endl;

    pcout << "INITIAL CONDITIONS" << std::endl;
    
    pcout << "Number of acoustic pulses: " << param.totalNumPulses << std::endl << std::endl;
    for (plint i = 0; i < param.totalNumPulses; i++) {
        pcout << "  pulse # " << i << ": " << std::endl;
        pcout << "    cx = " << param.cx[i] << std::endl;
        pcout << "    cy = " << param.cy[i]<< std::endl;
        pcout << "    eps = " << param.eps[i] << std::endl;
        pcout << "    b = " << param.b[i] << std::endl << std::endl;
    }

    pcout << "nu = " << param.nu << std::endl;
    pcout << "maxT = " << param.maxT << std::endl << std::endl;
    
    pcout << "Derived parameters: " << std::endl;
    pcout << "dx = " << param.dx << std::endl;
    pcout << "dt = " << param.dt << std::endl;
    pcout << "nuLB = " << param.nuLB << std::endl;
    pcout << "omega = " << param.omega << std::endl;
    
    pcout << "Physical Location = [" << param.physicalLocation[0] 
        << ", " << param.physicalLocation[1] << "] " << std::endl;
    pcout << "nx = " << param.nx << std::endl;
    pcout << "ny = " << param.ny << std::endl;
    pcout << "nx (physical domain) = " << param.nx_domain << std::endl;
    pcout << "ny (physical domain) = " << param.ny_domain << std::endl;

    pcout << std::endl;
    pcout << "Output directory = " << param.outputDir << std::endl;
    pcout << "Output mode = " << param.outputMode << std::endl;
    pcout << std::endl;
}

void computeDomainGeometry(Param& param)
{
    // Initially LB origin is at (0,0) pos in phys-units.
    // If a sponge zone is located at the bottom or left border,
    // the LB origin of the COMPLETE DOMAIN is moved resp. down or left.
    // The origin of the PHYSICAL DOMAIN (outside isothermal layers)
    // is ALWAYS at (0,0) in phys units.
    // This is to compute its position in LB units

    param.physicalLocation[0] = 0.0;
    param.physicalLocation[1] = 0.0;

    for (plint i = 0; i < 2; i++) {
        global::mpi().bCast(&param.physicalLocation[i], 1);
    }

}

void setRandomPulses(Param& param, std::mt19937& gen,
        std::uniform_int_distribution<plint>& distNum,
        std::uniform_real_distribution<T>& distCx,
        std::uniform_real_distribution<T>& distCy,
        std::uniform_real_distribution<T>& distEps,
        std::uniform_real_distribution<T>& distB )
{
    param.numRandPulses = distNum(gen);
    param.totalNumPulses = param.numRandPulses + param.numUserPulses;

    global::mpi().bCast(&param.numRandPulses, 1);
    global::mpi().bCast(&param.totalNumPulses, 1);

    std::vector<T> tmpCx(param.numRandPulses);
    std::vector<T> tmpCy(param.numRandPulses);
    std::vector<T> tmpEps(param.numRandPulses);
    std::vector<T> tmpB(param.numRandPulses);

    param.cx = param.userCx;
    param.cy = param.userCy;
    param.eps = param.userEps;
    param.b = param.userB;

    if (global::mpi().isMainProcessor()) {
        for (plint i = 0; i < param.numRandPulses; i++) {
            tmpCx[i] = distCx(gen);
            tmpCy[i] = distCy(gen);
            tmpEps[i] = distEps(gen);
            tmpB[i] = distB(gen);
        }
    }

    for (plint i = param.numUserPulses; i < param.totalNumPulses; i++) {
        global::mpi().bCast(&tmpCx[i],1);
        global::mpi().bCast(&tmpCy[i],1);
        global::mpi().bCast(&tmpEps[i],1);
        global::mpi().bCast(&tmpB[i],1);
    }
    
    if (param.numRandPulses > 0){
    	param.cx.insert(std::end(param.cx), std::begin(tmpCx), std::end(tmpCx));
    	param.cy.insert(std::end(param.cy), std::begin(tmpCy), std::end(tmpCy));
    	param.eps.insert(std::end(param.eps), std::begin(tmpEps), std::end(tmpEps));
    	param.b.insert(std::end(param.b), std::begin(tmpB), std::end(tmpB));
    }
}

Param param;

template<typename T>
class AdiabaticBC2D : public DomainFunctional2D {
public:
    AdiabaticBC2D()
    { 
        x1 = param.nx;
        y1 = param.ny;
    }
    virtual bool operator() (plint iX, plint iY) const {
        bool is_adiabatic = 0;
        if (iX < 1){ is_adiabatic = 1; } 
        if (iX >= x1 - 1){ is_adiabatic = 1; }
        if (iY < 1){ is_adiabatic = 1; }
        if (iY >= y1 - 1){ is_adiabatic = 1; }
        
        return is_adiabatic;
    }
    virtual AdiabaticBC2D<T>* clone() const {
        return new AdiabaticBC2D<T>(*this);
    }
private:
    T x0; T x1; T y0; T y1;
};


template<typename T>
class AdiabaticNodes{
public:
    AdiabaticNodes()
    { }
    bool operator() (plint iX, plint iY) const {
        return AdiabaticBC2D<T>()(iX, iY);
    }
};


/// Initialization of the temperature field.
template<typename T, template<typename U> class Descriptor>
struct IniTemperatureAdvectionDiffusionProcessor2D :
    public BoxProcessingFunctional2D_L<T,Descriptor> 
{
    IniTemperatureAdvectionDiffusionProcessor2D()
    { }
    virtual void process(Box2D domain, BlockLattice2D<T,Descriptor>& lattice)
    {
        Dot2D absoluteOffset = lattice.getLocation();
        
        plint absoluteX, absoluteY;
        double x, y, temperature;

        for (plint iX=domain.x0; iX<=domain.x1; ++iX) {
            for (plint iY=domain.y0; iY<=domain.y1; ++iY) {
                absoluteX = absoluteOffset.x + iX;
                absoluteY = absoluteOffset.y + iY;
                x = toPhys((T)absoluteX, 0, param.dx, param.physicalLocation);
                y = toPhys((T)absoluteY, 1, param.dx, param.physicalLocation);
                temperature = param.T0;
                
                for (plint i = 0; i < param.totalNumPulses; i++) {

                    if ( !param.pulseType.compare("gaussian") ) {
                        T alp = log(2.)/(param.b[i]*param.b[i]);
                        temperature += param.eps[i]*(
                            exp ( -alp*1.*(
                                    (x - param.cx[i])*(x - param.cx[i]) 
                                    +
                                    (y - param.cy[i])*(y - param.cy[i])
                                    ) 
                                )
                            );
                    }
                    else if ( !param.pulseType.compare("square") ){                        
                        if (abs(x - param.cx[i]) < param.b[i] && abs(y - param.cy[i]) < param.b[i]){
                            temperature += param.eps[i];
                        }
                    }
                }


                Array<T,Descriptor<T>::d> jEq(0.0, 0.0);
                lattice.get(iX,iY).defineDensity(temperature);
                iniCellAtEquilibrium(lattice.get(iX,iY), temperature, jEq);
            }
        }
    }
    virtual IniTemperatureAdvectionDiffusionProcessor2D<T,Descriptor>* clone() const
    {
        return new IniTemperatureAdvectionDiffusionProcessor2D<T,Descriptor>(*this);
    }
    
    virtual void getTypeOfModification(std::vector<modif::ModifT>& modified) const {
        modified[0] = modif::staticVariables;
    }
    
    virtual BlockDomain::DomainT appliesTo() const {
        return BlockDomain::bulkAndEnvelope;
    }
    
};

void advectionDiffusionSetup(MultiBlockLattice2D<T,DESCRIPTOR>& lattice, 
    OnLatticeAdvectionDiffusionBoundaryCondition2D<T,DESCRIPTOR>* bc)
{
    Box2D box = lattice.getBoundingBox();

    std::vector<Box2D> bc_list(4);
    Box2D left(box.x0, box.x0, box.y0, box.y1);
    Box2D right(box.x1, box.x1, box.y0, box.y1);
    Box2D bottom(box.x0+1, box.x1-1, box.y0, box.y0);
    Box2D top(box.x0+1, box.x1-1, box.y1, box.y1);

    bc_list[0] = left; 
    bc_list[1] = right; 
    bc_list[2] = bottom; 
    bc_list[3] = top;
    
    integrateProcessingFunctional (
        new FlatAdiabaticBoundaryFunctional2D<T,DESCRIPTOR,
        0, -1>, bc_list[0], lattice, 0);
    integrateProcessingFunctional (
        new FlatAdiabaticBoundaryFunctional2D<T,DESCRIPTOR,
        0, 1>, bc_list[1], lattice, 0);
    integrateProcessingFunctional (
        new FlatAdiabaticBoundaryFunctional2D<T,DESCRIPTOR,
        1, -1>, bc_list[2], lattice, 0);
    integrateProcessingFunctional (
        new FlatAdiabaticBoundaryFunctional2D<T,DESCRIPTOR,
        1, 1>, bc_list[3], lattice, 0);
    
    applyProcessingFunctional (
            new IniTemperatureAdvectionDiffusionProcessor2D<T,DESCRIPTOR>(), 
            lattice.getBoundingBox(),  lattice );
}

template< typename T,
          template<typename U1> class FluidDescriptor, 
          template<typename U2> class TemperatureDescriptor
        >
class AdvDiffCouplingProcessor2D :
    public BoxProcessingFunctional2D_LL<T,FluidDescriptor,T,TemperatureDescriptor>
{
public:
    
    AdvDiffCouplingProcessor2D();
    
    virtual void process( Box2D domain,
                          BlockLattice2D<T,FluidDescriptor>& fluid,
                          BlockLattice2D<T,TemperatureDescriptor>& temperature );
    virtual AdvDiffCouplingProcessor2D<T,FluidDescriptor,TemperatureDescriptor>* clone() const;
    virtual void getTypeOfModification(std::vector<modif::ModifT>& modified) const {
        modified[0] = modif::staticVariables;
        modified[1] = modif::staticVariables;
    }
    
};

template< typename T,
          template<typename U1> class FluidDescriptor, 
          template<typename U2> class TemperatureDescriptor
        >
AdvDiffCouplingProcessor2D<T,FluidDescriptor,TemperatureDescriptor>::
        AdvDiffCouplingProcessor2D()
{
}

template< typename T,
          template<typename U1> class FluidDescriptor, 
          template<typename U2> class TemperatureDescriptor
        >
void AdvDiffCouplingProcessor2D<T,FluidDescriptor,TemperatureDescriptor>::process (
        Box2D domain,
        BlockLattice2D<T,FluidDescriptor>& fluid,
        BlockLattice2D<T,TemperatureDescriptor>& temperature )
{
    typedef FluidDescriptor<T> D;
    enum {
        velOffset   = TemperatureDescriptor<T>::ExternalField::velocityBeginsAt,
        forceOffset = FluidDescriptor<T>::ExternalField::forceBeginsAt
    };
    Dot2D offset = computeRelativeDisplacement(fluid, temperature);
    
    for (plint iX=domain.x0; iX<=domain.x1; ++iX) 
    {
        for (plint iY=domain.y0; iY<=domain.y1; ++iY) 
        {
            T *u = temperature.get(iX+offset.x,iY+offset.y).getExternal(velOffset);
            Array<T,FluidDescriptor<T>::d> vel(0.,0.);
            vel.to_cArray(u);
            
            T *force = fluid.get(iX,iY).getExternal(forceOffset);
            for (pluint iD = 0; iD < D::d; ++iD) 
            {
                force[iD] = 0;;
            }
        }
    }
}

template< typename T,
          template<typename U1> class FluidDescriptor, 
          template<typename U2> class TemperatureDescriptor
        >
AdvDiffCouplingProcessor2D<T,FluidDescriptor,TemperatureDescriptor>*
    AdvDiffCouplingProcessor2D<T,FluidDescriptor,TemperatureDescriptor>::clone() const
{
    return new AdvDiffCouplingProcessor2D<T,FluidDescriptor,TemperatureDescriptor>(*this);
}

void writeVTKFull(MultiBlockLattice2D<T,DESCRIPTOR>& lattice, plint iter)
{
    T dx = param.dx;
    VtkImageOutput2D<T> vtkOut(createFileName("vtk", iter, 6), dx);
    vtkOut.writeData<float>(*computeDensity(lattice), "temperature", 1.);
}

void writeVTK(MultiBlockLattice2D<T,DESCRIPTOR>& lattice,plint iter)
{
    T dx = param.dx;
    
    T x0 = toLB((T) 0, 0, dx, param.physicalLocation);
    T x1 = x0 + param.nx_domain - 1;
    T y0 = toLB((T) 0, 1, dx, param.physicalLocation);
    T y1 = y0 + param.ny_domain - 1;
    
    Box2D box_phys(x0, x1, y0, y1);
    
    VtkImageOutput2D<T> vtkOut(createFileName("vtk", iter, 6), dx);
    auto temperature = extractSubDomain(*computeDensity(lattice), box_phys);
    vtkOut.writeData<float>(*temperature, "temperature", 1.);
}

typedef void (*writeFunction)(MultiBlockLattice2D<T,DESCRIPTOR>& lattice,
              plint iter);
writeFunction setVTKWriteFunction(string mode)
{
    if (mode.compare("full") == 0)
    {
        return writeVTKFull;
    }
    else if (mode.compare("crop") == 0)
    {
        return writeVTK;
    }
    else
    {
        std::string msg = "param.outputMode (" + mode + ") must be either 'full' or 'crop'.";
	    throw std::runtime_error(msg);
    }
}

// function for creating a formatted sring from an integer
std::string format_account_number(int acct_no) {
    char buffer[7];
    std::snprintf(buffer, sizeof(buffer), "%06d", acct_no);
    return buffer;
}

// function to create a folder
int mkpath(const char* file_path, mode_t mode) {
  assert(file_path && *file_path);
  char* p;
  char* fp;
  fp = const_cast<char*>(file_path);
  for (p=strchr(fp+1, '/'); p; p=strchr(p+1, '/')) {
    *p='\0';
    if (mkdir(fp, mode)==-1) {
      if (errno!=EEXIST) {
          *p='/'; return -1;
      }
    }
    *p='/';
  }
  return 0;
}



int main(int argc, char* argv[]) {
    plbInit(&argc, &argv);

    string xmlFileName;
    try {
        global::argv(1).read(xmlFileName);
    }
    catch (PlbIOException& exception) {
        pcout << "Wrong parameters; the syntax is: "
            << (std::string)global::argv(0) << " input-file.xml" << std::endl;
        return -1;
    }

    try {
        param = Param(xmlFileName);
    }
    catch (PlbIOException& exception) {
        pcout << exception.what() << std::endl;
        return -1;
    }
    global::IOpolicy().activateParallelIO(true);

    std::mt19937 gen(param.randomSeed);

    std::uniform_int_distribution<plint> uniformPulses(param.statNumRandPulses[0],
                param.statNumRandPulses[1]);
    std::uniform_real_distribution<T> uniformCx(param.statCx[0], param.statCx[1]);
    std::uniform_real_distribution<T> uniformCy(param.statCy[0], param.statCy[1]);
    std::uniform_real_distribution<T> uniformEps(param.statEps[0], param.statEps[1]);
    std::uniform_real_distribution<T> uniformB(param.statB[0], param.statB[1]);

    writeFunction vtkOuput = setVTKWriteFunction(param.outputMode); 

    // Loop over number of simulations (new initial conditions)
    for(plint sim=0; sim < param.numSim;  ++sim) { 
        std::string tmpOutDir = param.outputDir + '/' + format_account_number(sim) + '/';

        if (global::mpi().isMainProcessor()) {
            mkpath(tmpOutDir.c_str(), 0755);
        }

        computeDomainGeometry(param);
        setRandomPulses(
            param, gen,
            uniformPulses,
            uniformCx, uniformCy,
            uniformEps, uniformB);
        computeDerivedParameters(param);

        global::directories().setOutputDir(tmpOutDir + "/");
        
        pcout << "Initialising..." << std::endl;
        MultiBlockLattice2D<T, DESCRIPTOR> lattice (
                param.nx, param.ny, new DYNAMICS<T, DESCRIPTOR>(param.omega) );        
        MultiScalarField2D<T> adiabaticBCMask(param.nx, param.ny);

        lattice.toggleInternalStatistics(false);

        pcout << "Generating outer domain boundary conditions." << std::endl;
        OnLatticeAdvectionDiffusionBoundaryCondition2D<T,DESCRIPTOR>*
            bc = createLocalAdvectionDiffusionBoundaryCondition2D<T,DESCRIPTOR>();
        advectionDiffusionSetup(lattice, bc);
        delete bc; bc = 0;

        {
        MultiScalarField2D<int> flags(param.nx, param.ny);
        setToFunction(flags, flags.getBoundingBox(),
            AdiabaticNodes<T>()); 
        setToConstant(adiabaticBCMask, flags, 1, adiabaticBCMask.getBoundingBox(), 1.); 
        }
        
        lattice.initialize();

        pcout << "Initialisation finished!" << std::endl << std::endl;
        pcout << "Parameters :" << std::endl << std::endl;
        
        printParam(param);    

        // Main loop over time iterations.
        for (plint iT=0; iT<param.maxIter; ++iT) {
            if (iT% param.vtkIter == 0) {       
                pcout << "Iteration: " << iT*param.dt << endl;
                pcout << "Output vtk" << endl;
                vtkOuput(lattice, iT);
            }

            // Execute lattice Boltzmann iteration.
            lattice.collideAndStream();
        }

        param.cxLB.clear();
        param.cyLB.clear();

        param.cx.clear();
        param.cy.clear();
        param.eps.clear();
        param.b.clear();
        
        pcout << "End of run " << sim << std::endl << std::endl;

    }

}
