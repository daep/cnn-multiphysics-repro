#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Generates a 2D density field with a plane pulse (x in rows, y in columns)
Offers the possibility to save images, VTK files or a numpy file

VTK files are defined as to be read by the library. Example:
    
    ```
    nx, ny = 200, 200
    loader = pulsenetlib.preprocess.VTKLoader(['density'],nx,ny,1)
    data, _, _ = loader.load2D('vtk/file.vti')
    ```
    
"""

# native
##none##

# third-party
import numpy as np
import matplotlib.pyplot as plt
import vtk

# local modules
##none##

class AnalyticalWave():
    """ Analytical 1D wave pulse
    
    The solution is periodic of period MOD (method of images).
    Method `solve` returns a (1,nx) array with the solution for the input
    array, at time t.
    
    ----------
    ARGUMENTS
    
        amplitude: pulse amplitude
        half_width: pulse half-width
        velocity: pulse velocity
        length: domain length.=0
        mode: type of pulse (gaussian or square). Optional,
              'gaussian' as default
        dtype: array precision. Optional, default is np.double    
    
    """   
    def __init__(self, amplitude, half_width, velocity, length=0,
                 mode='gaussian', dtype=np.double):
        self.a = amplitude
        self.c = velocity
        self.hw = half_width
        self.L = length
            
        if mode == 'gaussian':
            self.F = self.f_1D_gaussian
        elif mode == 'square':
            self.F = self.f_1D_square
        else:
            raise ValueError("Unable to specify mode = " + mode +
                             ". Options are 'pulse' or 'square'.")

    def gaussian(self, x, mu, b):
        alp = np.log(2.) / b**2
        return np.exp(-alp * np.power(x - mu, 2.))

    def f_1D_gaussian(self, x):
        return self.a*(self.gaussian(x, self.L/2, self.hw)
                       + self.gaussian(x, -self.L/2, self.hw)
                       + self.gaussian(x, 3*self.L/2, self.hw))

    def f_1D_square(self, x):
        return self.a*(np.heaviside(x - (0.5*self.L - self.hw), 0.) \
                       - np.heaviside(x - (0.5*self.L + self.hw), 0.) \
                       + np.heaviside(x - (1.5*self.L - self.hw), 0.) \
                       - np.heaviside(x - (1.5*self.L + self.hw), 0.) \
                       + np.heaviside(x - (-0.5*self.L - self.hw), 0.) \
                       - np.heaviside(x - (-0.5*self.L + self.hw), 0.))

    def solve(self, x, t):
        return np.expand_dims(
            0.5*(self.F(x + self.c*t) + self.F(x - self.c*t)), axis=0) 



def save_VTK_2D(file_name, scalar, scalar_name, dx=1.0, dy=1.0):
    """ Function to save VTK file with a scalar
    
    The array is defined with x in rows and y in columns.
    Files can be read using pulsenetlib.preprocess.VTKLoader class.
    
    ----------
    ARGUMENTS
    
        file_name: path to the vtk file to be generated
        scalar: numpy 2D array with the scalar field
        scalar_name: string with the name
        dx, dy: grid spqcing in x and y. Optional, default is 1.0
        
    ----------
    RETURNS
    
        ##none##
    
    """

    nx, ny = scalar.shape
    
    image_data = vtk.vtkImageData()
    image_data.SetDimensions(nx,ny,1)
    image_data.SetOrigin(0.0,0.0,0.0)
    image_data.SetSpacing(dx,dy,0.0)
    image_data.AllocateScalars(vtk.VTK_DOUBLE, 1);
    
    for i in range(nx):
        for j in range(ny):
            for k in range(nz):
                image_data.SetScalarComponentFromDouble(
                    i, j, k, 0, scalar[i,j])
                
    image_data.GetPointData().GetScalars().SetName(scalar_name)
    
    vtk_writer = vtk.vtkXMLImageDataWriter()
    vtk_writer.SetFileName(file_name)    
    vtk_writer.SetInputData(image_data)
    vtk_writer.Write()



if __name__ == '__main__':
    # parameters for analytical solution
    amplitude = 1e-3 # amplitude of pulse
    mode = 'gaussian' # either gaussian or square (top-hat)
    half_width_dx = 12 # half-width over dx
    
    nx, ny = 200, 200
    domain_length = nx # 100
    nz = 1
    
    dx = domain_length/(nx + 1)
    dy = domain_length/(ny + 1)
    
    # base density
    rho0 = 1
    c_square = 1.0/3.0 # LBM speed of sound square
    c = c_square**0.5
    MOD = (domain_length/c - 1)
    half_width = half_width_dx * dx
    
    # simulation parameters
    number_timesteps = 600
    starting_timestep = 0
    delta_timestep = 1
    
    # save figures? and frequency (in number of timesteps)    
    export_fig = False
    plot_dt = 5
    if export_fig:
        # preparing plot
        fig, ax = plt.subplots()
        quad_mesh = ax.pcolormesh(np.zeros((nx,ny)), cmap='viridis',
                                  vmin=rho0, vmax=rho0 + 1.5*amplitude)
        ax.set_aspect(1.0)
        ax.set_xlabel('x')
        ax.set_ylabel('y')
        fig.colorbar(quad_mesh, ax=ax)
        plt.tight_layout()
    
    # save vtk files? and frequency
    export_vtk = True
    save_dt = 1
    
    # save *.npy file
    export_np = False
        
    w1d = AnalyticalWave(
        amplitude, half_width, c, domain_length, mode)
    # line where 1D solution is calculated
    X = np.linspace(0, domain_length, num=nx)
    density = np.zeros((number_timesteps, nx, ny))
    
    for timestep in range(0, number_timesteps):        
        idx = (starting_timestep + timestep*delta_timestep) % MOD
        # calcute initial frames and extrude it in the y-direction (columns)
        density_1D =  w1d.solve(X, idx) + rho0        
        density[timestep,:] = np.repeat(density_1D.T, ny, axis=1)
        
        print(timestep,':', np.max(density_1D))
        
        if export_fig and timestep % plot_dt == 0:
            # saving a figure with the field
            quad_mesh.set_array(density[timestep,:].T)
            fig.show()
            fig.savefig('figs/plane_wave.{:04d}.png'.format(timestep))
            
        if export_vtk and timestep % save_dt == 0:
            # saving a vtk file with the scalar field
            save_VTK_2D('benchmark/000002/vtk{:06d}.vti'.format(timestep),
                        density[timestep,:] - rho0, # saves acoustic density
                        'density',dx,dy)
            
    if export_np:
        # saving numpy fields with the solution
        np.save('plane_wave.npy',density - rho0)
        

        
    
